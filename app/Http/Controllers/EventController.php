<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\EventRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('date', 'DESC')->get();

        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $event = Event::create($request->all());

        $competitors = [];
        $controlls = [];

        $page = new Crawler(file_get_contents($request->get('url')));

        $courses = $this->parseCourses($page);

        $tables = $page->filter("thead");

        $tables->each(function(Crawler $table, $tableIndex) use (&$competitors, &$controlls) {
            $rows = $table->filter("tr");

            $rows->each(function(Crawler $row, $rowIndex) use (&$competitors, $tableIndex, &$controlls) {
                if($rowIndex === 0) return;
                if ($rowIndex === 1) {
                    $controlls[$tableIndex] = $this->parseControlls($row);
                }

                $cells = $row->filter("td");
                $cellCount = $cells->count();
                $cells->each(function(Crawler $cell, $cellIndex) use (&$competitors, $tableIndex, $rowIndex, $cellCount) {
                    if ($cellIndex === $cellCount - 1) { return; }
                    if ($cellIndex === 0) { $competitors["$tableIndex"]["$rowIndex"]['name'] = $cell->text(); }
                    if ($cellIndex === 1) { $competitors["$tableIndex"]["$rowIndex"]['group'] = $cell->text(); }
                    if ($cellIndex === 2) { $competitors["$tableIndex"]["$rowIndex"]['place'] = $cell->text(); }
                    if ($cellIndex === 3) { $competitors["$tableIndex"]["$rowIndex"]['time'] = $cell->text(); }
                    if ($cellIndex === 4) { $competitors["$tableIndex"]["$rowIndex"]['start'] = $cell->text(); }
                    if ($cellIndex > 4) {
                        $competitors["$tableIndex"]["$rowIndex"]['splits'][] = $this->parseSplit($cell->html());
                    }
                });
            });
        });

        $this->generateCsv($event, $competitors, $controlls, $courses);

        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function parseSplit($text)
    {
        $splits = explode('<br>', $text);
        foreach ($splits as $key => $split) {
            $s = explode(' ', $split);
            $splits[$key] = Arr::get($s, 0);
        }
        return $splits;
    }

    private function parseControlls(Crawler $row)
    {
        $controlls = [];
        $cells = $row->filter("th");
        $cells->each(function (Crawler $cell, $i) use (&$controlls) {
            if ($i <= 1) return;
            $controll = $cell->text();
            if ($controll === 'Finišas') {
                $controlls[] = "Fin";
                return;
            }
            $controll = explode(':', $controll);
            $controll = Arr::get($controll, 1);
            $converted = strtr($controll, array_flip(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES)));
            $controlls[] = trim($converted, chr(0xC2).chr(0xA0));
        });

        return $controlls;
    }

    private function generateCsv($event, $competitors, $controlls, $courses)
    {
        $fp = fopen(public_path("data/{$event->id}.csv"), 'w');

        $header = explode(',', 'Stno,SI card,Database Id,Name,YB,Block,nc,Start,Finish,Time,Classifier,Club no.,Cl.name,City,Nat,Cl. no.,Short,Long,Num1,Num2,Num3,Text1,Text2,Text3,Adr. name,Street,Line2,Zip,City,Phone,Fax,EMail,Id/Club,Rented,Start fee,Paid,Course no.,Course,km,m,Course controls,Pl,Start punch,Finish punch,Control1,Punch1,Control2,Punch2,Control3,Punch3,Control4,Punch4,Control5,Punch5,Control6,Punch6,Control7,Punch7,Control8,Punch8,Control9,Punch9,Control10,Punch10,(may be more) ...');
        fputcsv($fp, $header);

        foreach ($competitors as $courseKey => $course) {
            foreach ($course as $participant) {
                $row = [];
                for ($i = 0; $i < 44; $i++) { $row[$i] = ''; }

                $row[3] = Arr::get($participant, 'name');
                $start = Arr::get($participant, 'start');
                $row[7] = $start;
                $time = Arr::get($participant, 'time') === "DSQ" ? Arr::get(Arr::last(Arr::get($participant, 'splits')), 0) : Arr::get($participant, 'time');
                $row[8] = $this->getFinishTime($start, $time);
                $row[9] = $time;
                $row[10] = Arr::get($participant, 'time') === "DSQ" ? 3 : 0;
                $row[12] = 'N/A';
                $row[15] = Arr::get($participant, 'group');
                $row[16] = Arr::get($participant, 'group');
                $row[17] = Arr::get($participant, 'group');
                $row[36] = Arr::get($participant, 'group');
                $row[37] = Arr::get($participant, 'group');
                $row[38] = Arr::get($courses, "$courseKey.length");
                $row[40] = Arr::get($courses, "$courseKey.controlls");
                $row[41] = Arr::get($participant, 'place');

                $i = 44;
                foreach (Arr::get($participant, 'splits') as $splitKey => $split) {
                    $c = Arr::get($controlls, "$courseKey.$splitKey");
                    $row[$i] = $c;
                    $i++;
                    $t = Arr::get($split, '0');
                    $row[$i] = $t;
                    $i++;
                }

                fputcsv($fp, $row);
            }
        }

        fclose($fp);
    }

    private function getFinishTime($start, $time)
    {
        try {
            $time = explode(":", $time);
            if (count($time) === 2) {
                return '00:' . implode(":", $time);
            } elseif (count($time) === 3) {
                if ($time[0] > 9) {
                    return implode(":", $time);
                }
                return "0" . implode(":", $time);
            }
            $time = implode(":", $time);

            $secs = strtotime($time) - strtotime("00:00:00");
            $result = date("H:i:s", strtotime($start) + $secs);
        } catch (\Exception $e) {
            return "00:00:00";
        }

        return $result;
    }

    private function parseCourses(Crawler $page)
    {
        $courses = [];
        $bs = $page->filter("b");
        $bs->each(function(Crawler $b) use (&$courses) {
            $text = $b->text();
            if (Str::contains($text, 'km') && Str::contains($text, 'KP')) {
                $text = str_replace(" KP", '', $text);
                $text = explode(" km ", $text);
                $courses[] = [
                    'length' => (float) $text[0],
                    'controlls' => (int) $text[1],
                ];
            }
        });

        return $courses;
    }
}
