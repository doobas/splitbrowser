<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name', 'date', 'url', 'meta'
    ];

    protected $dates = ['date'];

    protected $casts = [
        'meta' => 'array'
    ];
}
