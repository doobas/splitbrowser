
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <title>Splitai</title>
    <script type="text/javascript" charset="utf-8" src="/js/modernizr-2.7.1-svg.min.js"></script>
    <script type="text/javascript">
      if (!Modernizr.svg) {
        document.location = "unsupported-browser.html";
      }
    </script>
    <script type="text/javascript" charset="utf-8" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/d3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/splitsbrowser.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/lang/messages-en_gb.js"></script>
</head>
<body>


<script type="text/javascript">
  SplitsBrowser.loadEvent('/data/{{ $event->id }}.csv');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129246416-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-129246416-2');
</script>


<noscript>
    <h1>SplitsBrowser &ndash; JavaScript is disabled</h1>

    <p>Your browser is set to disable JavaScript.</p>

    <p>SplitsBrowser cannot run if JavaScript is not enabled.  Please enable JavaScript in your browser.</p>
</noscript>
</body>
</html>
