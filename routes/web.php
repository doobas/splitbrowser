<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EventController@index')->name('events.index');
Route::get('/events/{event}', 'EventController@show')->name('events.show');
Route::get('/super_secret_address', 'EventController@create');
Route::post('/super_secret_address', 'EventController@store')->name('events.store');
